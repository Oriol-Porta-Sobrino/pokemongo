import pymongo

from pymongo import MongoClient

from pprint import pprint

uri = "mongodb+srv://m001-student:super3@sandbox.gpjyw.mongodb.net/project?retryWrites=true&w=majority"
client = MongoClient(uri)

db = client["project"]

def query(lineaEntrada) :
    print("Modo Consulta...")
    if len(lineaEntrada) == 3 :
        pokemonEntrada = lineaEntrada[1]   
        pokemonDato = lineaEntrada[2]         
        if str.isdigit(pokemonEntrada) :
            docPokemon = db.pokemon.find_one({"num":str(pokemonEntrada).zfill(3)})
        else :
            docPokemon = db.pokemon.find_one({"name":pokemonEntrada})
        if docPokemon == None :
            print('No existe el pokemon')
        else :           
            try:
                tuplaPokemon = (docPokemon[pokemonDato])
                print(pokemonDato, "de", docPokemon["name"])
                pprint(tuplaPokemon)
            except:
                print("No tiene este campo")
    else :
        print("Introduce 1 entrada con 3 campos")

def detach(lineaEntrada) :
    print('Modo borrar...')
    if len(lineaEntrada) == 3 :
        nombreTeam = lineaEntrada[1]  
        if nombreTeam != "team" :
            print("Tienes que añadir los pokemon a 'team'")
        else :      
            pokemonEntrada = lineaEntrada[2]
            if str.isdigit(pokemonEntrada) :
                delPokemon = db.team_pokemon.find_one({"num":str(pokemonEntrada).zfill(3)})
            else :
                delPokemon = db.team_pokemon.find_one({"name":pokemonEntrada})
            if delPokemon == None :
                print("No existe el pokemon")
            else :
                db.team.find_one_and_update({"name": nombreTeam}, {'$pull': {'pokemons': delPokemon["_id"]}})
                print("Se ha borrado el pokemon", delPokemon["name"], "correctamente")
    else :
        print("Introduce 1 entrada en 3 campos")

def score(lineaEntrada) :
    print('Modo score...')
    if len(lineaEntrada) == 2:
        usuario = lineaEntrada[1]
        antes = db.player.find_one_and_update({'alias': usuario}, {'$inc': {'score': 5}})
        usuarioFind = db.player.find_one({'alias': usuario})
        if usuarioFind != None :
            print("Punctuació augmentada a" , usuarioFind['alias'] , "de", antes["score"], "a", usuarioFind['score'] , ' punts')
        else :
            print("No existe el usuario", usuario)
    else :
        print("Introduce una entrada en 2 campos")

def addUser(lineaEntrada) :
    print('Modo adduser...')
    if len(lineaEntrada) == 3 :
        nombreTeam = lineaEntrada[1]
        if nombreTeam != 'team' :
            print("Tienes que añadir el usuario a 'team'")
        else :
            alias = lineaEntrada[2]
            usuario = db.player.find_one({"alias": alias})
            if usuario == None :
                print("No existe el usuario" , alias)
            else :
                team = db.team.find_one({"name": nombreTeam})
                db.team.find_one_and_update({"name": nombreTeam}, {'$set': {'player': usuario["_id"]}})
                team = db.team.find_one({"name": nombreTeam})
                print("Añadido usuario", usuario["alias"], "a equipo", team["name"])
    else :
        print("Introduce una entrada en 3 campos")


