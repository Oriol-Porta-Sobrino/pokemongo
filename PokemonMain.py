import PokemonPy as PokemonPy
import PokemonEngine as PokemonEngine

while (True):
    print("Introdueix la comanda:")
    lineaEntrada = input().split()
    if lineaEntrada[0].lower() == "fin":
        break
    elif lineaEntrada[0].lower() == "query":
        PokemonPy.query(lineaEntrada)
    elif lineaEntrada[0].lower() == "add":
        PokemonEngine.addPokemon(lineaEntrada)
    elif lineaEntrada[0].lower() == "detach":
        PokemonPy.detach(lineaEntrada)
    elif lineaEntrada[0].lower() == 'score' :
        PokemonPy.score(lineaEntrada)
    elif lineaEntrada[0].lower() == "adduser" :
        PokemonPy.addUser(lineaEntrada)
    elif lineaEntrada[0].lower() == 'combat' :
        PokemonEngine.combat()
    else:
        print("Despiertaaaa!! Pon un comando correcto, hombre!!!!")
print("Fin del programa!")