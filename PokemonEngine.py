from mongoengine import *
from random import randrange
from datetime import datetime
#from inspect import getmembers, isfunction
from pprint import pprint

TYPES = ('Bug','Dark','Dragon','Electric','Fairy','Fighting','Fire','Flying','Ghost','Grass','Ground','Ice','Normal','Poison','Psychic','Rock','Steel','Water')

uri = "mongodb+srv://m001-student:super3@sandbox.gpjyw.mongodb.net/project?retryWrites=true&w=majority"
client = connect(host=uri)

db = client["project"]

class Move(Document):
    meta = {'allow_inheritance': True}
    name = StringField(requiered = True)
    pwr = IntField(requiered = True, min_value=0, max_value=180)
    tipus = StringField(choices=TYPES, requiered = True)

class FastMove(Move):
    energyGain = IntField(requiered = True, min_value=0, max_value=35)

class ChargedMove(Move):
    energyCost = IntField(requiered = True, min_value=33, max_value=110)

class Moves(EmbeddedDocument):
    fast = LazyReferenceField(FastMove)
    charged = LazyReferenceField(ChargedMove)

class TeamPokemon(Document):
    num = StringField()
    name = StringField()
    ptype = ListField(db_field="type",choices=TYPES, required = True)
    moves = EmbeddedDocumentField(Moves)
    weaknesses = ListField(choices=TYPES, required = True)
    hp_max = IntField(min_value = 200, max_value = 1000)
    atak = IntField(min_value = 10, max_value = 50)
    defense = IntField(min_value = 10, max_value = 50)
    cp = IntField()    
    hp = IntField()
    energy = IntField()
    add_date = DateField()

class Player(Document):
    nom = StringField()
    surname = StringField()
    alias = StringField()
    score = IntField()

class Team(Document):
    player = LazyReferenceField(Player)
    name = StringField()
    pokemons = ListField(LazyReferenceField(TeamPokemon))

class Evolution(EmbeddedDocument):
    num = StringField()
    name = StringField()

class Pokemon(Document):
    pid = IntField(required = True, db_field='id')
    num = StringField(required = True)
    name = StringField(required = True)
    img = StringField(required = True)
    type = ListField(choice = TYPES, required = True)
    height = StringField(required = True)
    weight = StringField(required = True)
    candy = StringField(required = True)
    candy_count = FloatField(min_value = 0, max_value = 500)
    egg = StringField(required = True)
    spawn_chance = FloatField(required = True)
    avg_spawns = FloatField(required = True)
    spawn_time = StringField(required = True)
    multipliers = ListField(FloatField(),required = True)
    weaknesses = ListField(choice = TYPES,required = True)
    next_evolution = ListField(EmbeddedDocumentField('Evolution'))
    prev_evolution = ListField(EmbeddedDocumentField('Evolution'))

def llenarBase() :

    steelwing = FastMove(name = "Steel Wing",pwr = 11, tipus = "Steel")
    steelwing.energyGain=6
    steelwing.save()

    dragonTail = FastMove(name = "Dragon Tail",pwr = 15, tipus = "Dragon")
    dragonTail.energyGain=9
    dragonTail.save()

    razorLeaf = FastMove(name = "Razor Leaf", pwr = 13, tipus = 'Grass')
    razorLeaf.energyGain=7
    razorLeaf.save()

    counter = FastMove(name = "Counter", pwr = 12, tipus = "Fighting")
    counter.energyGain = 8
    counter.save()

    waterfall = FastMove(name = "Waterfall", pwr = 16, tipus = "Water")
    waterfall.energyGain = 8
    waterfall.save()

    fireSpin = FastMove(name = "Fast Spin", pwr = 14, tipus = "Fire")
    fireSpin.energyGain = 10
    fireSpin.save()

    doomdesire = ChargedMove(name = "Doom Desire",pwr = 80, tipus = "Steel")
    doomdesire.energyCost=50
    doomdesire.save()

    dracoMeteor = ChargedMove(name = "Draco Meteor",pwr = 150, tipus = "Dragon")
    dracoMeteor.energyCost=100
    dracoMeteor.save()

    stoneEdge = ChargedMove(name = "Stone Edge", pwr = 100, tipus = "Rock")
    stoneEdge.energyCost = 100
    stoneEdge.save()

    blizzard = ChargedMove(name = "Blizzard", pwr = 130, tipus = "Ice")
    blizzard.energyCost = 100
    blizzard.save()

    thunder = ChargedMove(name = "Thunder", pwr = 100, tipus = "Electric")
    thunder.energyCost = 100
    thunder.save()

    hurricane = ChargedMove(name = "Hurricane", pwr = 110, tipus = "Flying")
    hurricane.energyCost = 100
    hurricane.save()

    player1 = Player(nom = "Ash",
    surname = "Mayonesam",
    alias = "Rojo",
    score = 0) 
    
    player2 = Player(nom = "Marco",
    surname = "Polo",
    alias = "Azul",
    score = 0) 

    player1.save()
    player2.save()

    team = Team(
    name = "team"
    )

    rapidoList = FastMove.objects
    cargadoList = ChargedMove.objects

    
    i = 12
    while i > 0 :
        ale = randrange(0, len(rapidoList))
        rapido = rapidoList[ale]
        ale = randrange(0, len(cargadoList))
        cargado = cargadoList[ale]
        moves = Moves(
            fast = rapido,
            charged = cargado
        )
        rnd = randrange(1, 150)
        num = str(rnd).zfill(3)
        addPokemonFind = Pokemon.objects(num = num).limit(1) 
        addPokemon = addPokemonFind[0]
        addHpMax = randrange(200, 1000)
        addAtak = randrange(10, 50)
        addDefense = randrange(10, 50)
        addHp = addHpMax
        addCp = addHpMax+addAtak+addDefense
        addAdd_Date = datetime.now()
        
    
        addTeamPokemon = TeamPokemon(num = addPokemon.num,
            name = addPokemon.name,
            ptype = addPokemon.type,
            moves = moves,
            hp_max = addHpMax,
            atak = addAtak,
            defense = addDefense,
            hp = addHp,
            energy = 0,
            cp = addCp,
            add_date = addAdd_Date,
            weaknesses = addPokemon.weaknesses
        )
        addTeamPokemon.save()
        i -= 1
        team.pokemons.append(addTeamPokemon)
        team.save()



def addPokemon(lineaEntrada) :
    print("Modo Añadir...")
    if len(lineaEntrada) == 3:
        nombreTeam = lineaEntrada[1]
        if nombreTeam != "team" :
            print("Tienes que añadir los pokemon a 'team'")
        else :
            teamFind = Team.objects(name = nombreTeam).limit(1)
            team = teamFind[0]
            nombrePokemon = lineaEntrada[2]
            addPokemonFind = Pokemon.objects(name = nombrePokemon).limit(1)       
            if not addPokemonFind :
                print('No existe ese pokemon')
            else :
                addPokemon = addPokemonFind[0]
                anadirMov = True
                while anadirMov :
                    print('Añade movimiento rapido')
                    print('Movimientos disponibles')
                    for i in FastMove.objects :
                        print(i.name)
                    print('Selecciona movimiento')
                    movimientoRapido = input()
                    addMovimientoRapidoFind = FastMove.objects(name = movimientoRapido).limit(1)              
                    if not addMovimientoRapidoFind :
                        print('Ese movimiento no existe')
                    else :
                        addMovimientoRapido = addMovimientoRapidoFind[0]
                        anadirMov = False

                anadirMov = True
                while anadirMov :
                    print('Añade movimiento cargado')
                    print('Movimientos disponibles')
                    for i in ChargedMove.objects :
                        print(i.name)
                    print('Selecciona movimiento')
                    movimientoCargado = input()
                    addMovimientoCargadoFind = ChargedMove.objects(name = movimientoCargado).limit(1)               
                    if not addMovimientoCargadoFind :
                        print('Ese movimiento no existe')
                    else :
                        addMovimientoCargado = addMovimientoCargadoFind[0]
                        anadirMov = False

                moves = Moves(
                    fast = addMovimientoRapido,
                    charged = addMovimientoCargado
                )

                addHpMax = randrange(200, 1000)
                addAtak = randrange(10, 50)
                addDefense = randrange(10, 50)
                addHp = addHpMax
                addCp = addHpMax+addAtak+addDefense
                addAdd_Date = datetime.now()
                addTeamPokemon = TeamPokemon(num = addPokemon.num,
                    name = addPokemon.name,
                    ptype = addPokemon.type,
                    moves = moves,
                    hp_max = addHpMax,
                    atak = addAtak,
                    defense = addDefense,
                    hp = addHp,
                    energy = 0,
                    cp = addCp,
                    add_date = addAdd_Date,
                    weaknesses = addPokemon.weaknesses
                )
                addTeamPokemon.save()
                team.pokemons.append(addTeamPokemon)
                team.save()
    else :
        print("Introduce una entrada en 3 campos")

def combat() :
    print("Modo combate...")
    nombrePlayer1 = "Ash"
    nombrePlayer2 = "Marco"
    player1 = Player.objects(nom = nombrePlayer1)
    player2 = Player.objects(nom = nombrePlayer2)
    max = 6
    lista1 = TeamPokemon.objects().limit(max)
    lista2 = TeamPokemon.objects().skip(max).limit(max*2)
    if len(Team.objects) > 1 :
        equipos = Team.objects
        TeamA = equipos[1]
        TeamB = equipos[2]
        TeamA.pokemons = lista1
        TeamB.pokemons = lista2
        TeamA.save()
        TeamB.save()
    else :
        TeamA = Team(player = player1[0],
            name = "TeamA",
            pokemons = lista1)
        TeamA.save()
        TeamB = Team(player = player2[0],
            name = "TeamB",
            pokemons = lista2)
        TeamB.save()
    pokemonAFind = TeamPokemon.objects(id = TeamA.pokemons[0].id)
    pokemonBFind = TeamPokemon.objects(id = TeamB.pokemons[0].id)
    pokemonA = pokemonAFind[0]
    pokemonB = pokemonBFind[0]
    turno = 0
    posA = 0
    posB = 0
    ganador = 0
    while ganador == 0 :
        muerto = False
        if turno == 0 :
            dueno = TeamA
            rival = TeamB
            pokemonAFind = TeamPokemon.objects(id = TeamA.pokemons[posA].id)
            pokemonBFind = TeamPokemon.objects(id = TeamB.pokemons[posB].id)
            pokemonA = pokemonAFind[0]
            pokemonB = pokemonBFind[0]
        elif turno == 1 :
            dueno = TeamB
            rival = TeamA
            pokemonAFind = TeamPokemon.objects(id = TeamA.pokemons[posA].id)
            pokemonBFind = TeamPokemon.objects(id = TeamB.pokemons[posB].id)
            pokemonA = pokemonBFind[0]
            pokemonB = pokemonAFind[0]
        print("Es el turno de " , dueno.name)
        print("No es el turno de " , rival.name)
        print("Turno del pokemon " , pokemonA.name)
        print("Pokemon rival " , pokemonB.name)
        print("Ataque || Cambio")
        
        rnd = randrange(1, 4)
        if rnd == 1 :
            ordenOpcion = "cambio"
        else :
            ordenOpcion = "ataque"
        #ordenOpcion = input()
        if ordenOpcion.lower() == "ataque" :
            print("Has entrado en ataque")
            print("Fast || Charged")
            rnd = randrange(1, 2)
            if rnd == 1 :
                ordenAtaque = "fast"
            elif rnd == 2 :
                ordenAtaque = "charged"
            #ordenAtaque = input()
            if ordenAtaque.lower() == "fast" :
                print("Has seleccionado ataque rapido")
                ataqueFind = Move.objects(id = pokemonA.moves.fast.id)
                ataque = ataqueFind[0]
                dano = (5 * pokemonA.atak * ataque.pwr) / (pokemonB.defense * 2)
                if ataque.tipus in pokemonB.weaknesses :
                    dano = dano * 2
                    print("Es muy eficaz")
                if ataque.tipus in pokemonA.ptype :
                    dano = dano * 1.5
                    print("Bonus por tipo")
                print("El daño es " , dano)
                print("Vida de", pokemonB.name, "antes del ataque" , pokemonB.hp)
                pokemonB.hp -= dano
                print("Vida de", pokemonB.name, "despues del ataque" , pokemonB.hp)
                if pokemonB.hp <= 0 :
                    #se quita el pokemon del equipo
                    rival.pokemons.remove(pokemonB)
                    rival.save()         
                    print(pokemonB.name ," se ha debilitado")  
                    ordenOpcion = "cambio"   
                    muerto = True   
                    if turno == 0 :
                        jugadorFind = Player.objects(id = TeamA.player.id)
                        jugador = jugadorFind[0]
                        jugador.score += 5
                        jugador.save()
                    elif turno == 1 :
                        jugadorFind = Player.objects(id = TeamB.player.id)
                        jugador = jugadorFind[0]
                        jugador.score += 5
                        jugador.save()  
                pokemonA.energy += ataque.energyGain
                pokemonA.save()
                pokemonB.save()
            elif ordenAtaque.lower() == "charged" :
                print("Has seleccionado ataque cargado")
                ataqueFind = Move.objects(id = pokemonA.moves.charged.id)
                ataque = ataqueFind[0]
                if ataque.energyCost > pokemonA.energy :
                    print("No tienes energia suficiente")
                    print("El ataque ha fallado")
                else :
                    print("Tienes energia suficiente")
                    dano = 5 * pokemonA.atak * ataque.pwr / pokemonB.defense * 2
                if ataque.tipus in pokemonB.weaknesses :
                    dano = dano * 2
                    print("Es muy eficaz")
                if ataque.tipus in pokemonA.ptype :
                    dano = dano * 1.5
                    print("Bonus por tipo")
                print("El daño es " , dano)
                print("Vida del pokemon enemigo antes del ataque " , pokemonB.hp)
                pokemonB.hp -= dano
                print("Vida del pokemon enemigo despues del ataque " , pokemonB.hp)
                if pokemonB.hp <= 0 :
                    #se quita el pokemon del equipo
                    rival.pokemons.remove(pokemonB)
                    rival.save()         
                    print("El pokemon enemigo se ha debilitado")       
                    ordenOpcion = "cambio"   
                    muerto = True
                    if turno == 0 :
                        jugadorFind = Player.objects(id = TeamA.player.id)
                        jugador = jugadorFind[0]
                        jugador.score += 5
                        jugador.save()
                    elif turno == 1 :
                        jugadorFind = Player.objects(id = TeamB.player.id)
                        jugador = jugadorFind[0]
                        jugador.score += 5
                        jugador.save() 
                pokemonA.energy -= ataque.energyCost
                pokemonA.save()
                pokemonB.save()                               
            else :
                print("El comando tiene que ser 'Fast' o 'Charged'")
        if len(rival.pokemons) == 0 :
            ordenOpcion = "no"
            if turno == 0 :
                ganador = 1
            elif turno == 1 :
                ganador = 2
        if ordenOpcion.lower() == "cambio" :
            print("Has entrado en cambio")
            n = 0
            if muerto :
                listaPokemon = rival.pokemons
                for i in listaPokemon :
                    poke = TeamPokemon.objects(id = rival.pokemons[n].id)
                    print(n,": " , poke[0].name)
                    n += 1 
            else :
                listaPokemon = dueno.pokemons
                for i in listaPokemon :
                    poke = TeamPokemon.objects(id = dueno.pokemons[n].id)
                    print(n,": " , poke[0].name)
                    n += 1 
            
            print("Escoje un pokemon")
            
            cambio = randrange(0, len(listaPokemon))
            #cambio = input()  
            #cambiar pokemon  
            if turno == 0 :
                if muerto :
                    posB = cambio
                else:
                    posA = cambio
            elif turno == 1 :
                if muerto :
                    posA = cambio
                else :
                    posB = cambio  
            print("Has cambiado de pokemon")
        elif ordenOpcion.lower() != "cambio" and ordenOpcion.lower() != "ataque":
            print("El comando tiene que ser 'Ataque' o 'Cambio'")
        if turno == 0 :
            turno = 1
        elif turno == 1 :
            turno = 0 
    if ganador == 1 :
        print("El ganador es " , TeamA.name)
    elif ganador == 2 :
        print("El ganador es " , TeamB.name)    
    revivir()
    
def revivir() :
    for i in TeamPokemon.objects :
        addHpMax = randrange(200, 1000)
        addAtak = randrange(10, 50)
        addDefense = randrange(10, 50)
        addHp = addHpMax
        addCp = addHpMax+addAtak+addDefense
        i.hp_max = addHpMax
        i.atak = addAtak
        i.defense = addDefense
        i.hp = addHp
        i.energy = 0
        i.cp = addCp
        i.save()
            

db.move.drop()
db.team_pokemon.drop()
db.team.drop()
db.player.drop()

llenarBase()








    